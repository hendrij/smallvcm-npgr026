#!/bin/bash

# parameters: #seconds

mkdir -p results/beauty

./smallvcm -s 4 -a bpt -t $1 -maxlen  2 -mabs 0.0 -mscat 0.0 -o results/beauty/s4-nomedium-l02.hdr
./smallvcm -s 4 -a bpt -t $1 -maxlen 40 -mabs 0.0 -mscat 0.0 -o results/beauty/s4-nomedium-l40.hdr

./smallvcm -s 4 -a bpt -t $1 -maxlen  2 -mabs 0.1 -mscat 0.0 -o results/beauty/s4-mabs-l02.hdr
./smallvcm -s 4 -a bpt -t $1 -maxlen 40 -mabs 0.1 -mscat 0.0 -o results/beauty/s4-mabs-l40.hdr

./smallvcm -s 4 -a bpt -t $1 -maxlen  2 -mabs 0.1 -mscat 0.2 -o results/beauty/s4-mscat-l02.hdr
./smallvcm -s 4 -a bpt -t $1 -maxlen 40 -mabs 0.1 -mscat 0.2 -o results/beauty/s4-mscat-l40.hdr
