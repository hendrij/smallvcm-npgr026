# This is under MIT licence
# Also, I am not at all proud of this makefile, feel free to make better

all:
	g++ -o smallvcm ./src/smallvcm.cxx -O3 -std=c++11 -fopenmp

debug:
	g++ -o smallvcm ./src/smallvcm.cxx -g -O0 -std=c++11 -fopenmp

old_rng:
	g++ -o smallvcm ./src/smallvcm.cxx -O3 -std=c++11 -fopenmp -DLEGACY_RNG

clean:
	rm smallvcm

unreport:
	rm *.bmp index.html
